import {Component} from '@angular/core';
import {Users} from './table/users.model';
/*import {HttpClientModule, HttpClient} from '@angular/common/http';*/
import 'rxjs/add/operator/map';
/*import {map} from 'rxjs/operators';
import {Observable, Subject} from 'rxjs';*/
import 'rxjs/add/operator/catch';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  users = [
    new Users('Max', 'Roma', 33),
    new Users('Mario', 'Milano', 25),
    new Users('Simona', 'Napoli', 22)
  ];
  /*users: Users[];

  constructor(private http: HttpClient) {
    return this.http.get<>('assets/user.json');
  }*/
}
